import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

import { Comment } from '../../models/comment';
import { PostsService } from '../../services/posts.services';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment;

  constructor(public postsService: PostsService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  public vote(vote: boolean, comment: Comment) {
    this.postsService.updateComment(comment.postId, comment.id, vote)
      .then(() => {
        const message = vote ? 'like' : 'don\'t';
        this.snackBar.open(`You ${message} this comment.`, null, { duration: 3000 });
      })
      .catch((e: HttpErrorResponse) => {
        this.snackBar.open(e.message, null, { duration: 3000 });
      });
  }
}
