import { Component, Input} from '@angular/core';
import { Router } from '@angular/router';

import { Post } from '../../models/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent {

  @Input() post: Post;

  constructor(private router: Router) {}

  public goToPost(postId: number): void {
    this.router.navigate(['post', postId]);
  }

}
