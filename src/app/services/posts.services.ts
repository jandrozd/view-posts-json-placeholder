import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/internal/operators';

import { Post } from '../models/post';
import { Comment } from '../models/comment';

const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts';
const LIMIT = 4;

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=UTF-8',
    })
  };
  public IN_PROGRESS_NONE = 0;
  public IN_PROGRESS_LOAD_POSTS = 1;
  public inProgress = new BehaviorSubject<number>(this.IN_PROGRESS_NONE);
  public postsList = new BehaviorSubject<Post[]>([]);
  public postComments = new BehaviorSubject<Comment[]>([]);
  private startPage = 1;

  constructor(private http: HttpClient) {
    this.getPosts();
  }

  public getPosts(): Promise<boolean> {
    this.inProgress.next(this.IN_PROGRESS_LOAD_POSTS);
    const apiUrl = `${POSTS_URL}?_embed=comments&_sort=views&_order=desc&_start=${(this.startPage - 1) * LIMIT}&_end=${this.startPage * LIMIT}`;
    return this.http.get<Post[]>(apiUrl)
      .pipe(
        tap(result => {
          this.inProgress.next(this.IN_PROGRESS_NONE);
          this.startPage++;
          return result;
        })
      )
      .toPromise()
      .then(posts => {
        this.postsList.next(this.postsList.value.concat(posts));
        return true;
      })
      .catch(() => { return false; });
  }

  public getPostComments(postId: number, page: number): void {
    const comments = this.postsList.value?.filter(post => post.id === postId)[0]?.comments;
    this.postComments.next( this.postComments.value.concat(this.paginateArray(comments, LIMIT, page)));
  }

  public clearPostComments(): void {
    this.postComments.next([]);
  }

  public updateComment(postId, commentId, vote) {
    const comments = this.postComments.value;
    const commentIndex = comments.findIndex(comment => comment.id == commentId);
    comments[commentIndex].like = vote;
    this.postComments.next(comments);
    const post = this.postsList.value.filter(post => post.id === postId)[0];
    return this.http.patch<Post>(`${POSTS_URL}/${postId}`, JSON.stringify(post), this.httpOptions).toPromise();
  }

  private paginateArray(array, page_size, page_number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
  }

}
