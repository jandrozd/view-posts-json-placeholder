import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { PostsService } from '../services/posts.services';

@Injectable({
  providedIn: 'root'
})
export class CommentsResolver implements Resolve<void> {

  constructor(private postsService: PostsService) { }

  resolve(route: ActivatedRouteSnapshot): void {
    if(this.postsService.postsList.value.length) {
      this.loadPostComments(route.params['postId']);
    } else {
      this.postsService.getPosts().then(() => {
        this.loadPostComments(route.params['postId']);
      });
    }
  }

  private loadPostComments(postId): void {
    this.postsService.getPostComments(parseInt(postId), 1);
  }
}
