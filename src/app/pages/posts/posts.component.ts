import { Component, OnInit } from '@angular/core';

import { PostsService } from '../../services/posts.services';
import { Post } from '../../models/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: Array<Post>;
  inProgress: number = 0;

  constructor(public postsService: PostsService) {}

  ngOnInit(): void {
    this.postsService.postsList?.asObservable().subscribe(posts => {
      this.posts = posts;
    });

    this.postsService.inProgress?.asObservable().subscribe(num => {
      this.inProgress = num;
    });

    this.postsService.clearPostComments();
  }

  public loadMorePosts() {
    this.postsService.getPosts();
  }
}
