import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs/index';
import { takeUntil } from 'rxjs/internal/operators';

import { PostsService } from '../../services/posts.services';
import { Post } from '../../models/post';
import { Comment } from '../../models/comment';

const LIMIT = 4;

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, OnDestroy {
  public post: Post;
  public postId: string;
  public comments: Array<Comment>;
  public page = 2; // 1 - is in resolver
  public loadMoreButtonHidden: boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    public postsService: PostsService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    this.postId = this.route.snapshot.paramMap.get('postId');
  }

  ngOnInit(): void {
    this.postsService.postsList?.asObservable()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(posts => {
      if (posts.length) {
        this.post = posts.filter(post => {
          return post.id === parseInt(this.postId);
        })[0];

        if (!this.post) {
          this.snackBar.open('This post doest exist. Try load more posts and find this one.', null, { duration: 3000 });
          this.back();
        }
      }
    });

    this.postsService.postComments?.asObservable()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(comments => {
      this.comments = comments;
    });
  }

  ngOnDestroy(): void {
    this.postsService.clearPostComments();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public loadMoreComments() {
    this.postsService.getPostComments(parseInt(this.postId), this.page);
    this.page++;

    if(this.post.comments.length < this.page * LIMIT) {
      this.loadMoreButtonHidden = true;
    }
  }

  public back(): void {
    this.router.navigate(['']);
  }

}
